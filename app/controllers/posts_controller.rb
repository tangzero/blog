class PostsController < InheritedResources::Base
  before_filter :authenticate_admin!, :except => [:index, :show]
  respond_to :atom, :only => [:index]

  def index
    @posts = Post.recent.tagged(params[:tag]).paginate(:page => params[:page], :per_page => 5)
    index!
  end

  def create
    @post = Post.new(params[:post])
    @post.admin = current_admin
    create!
  end

end
