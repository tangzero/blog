class Post < ActiveRecord::Base
  belongs_to :admin
  validates_presence_of :title, :body
  has_friendly_id :title, :use_slug => true
  acts_as_taggable

  scope :tagged, lambda { |tag| tagged_with(tag) }
  scope :recent, order("created_at desc")
end
