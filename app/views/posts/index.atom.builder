atom_feed do |feed|
  feed.title "Jairo Luiz Blog"
  feed.updated @posts.last.created_at
 
  for post in @posts
    feed.entry(post) do |entry|
      entry.title post.title
      entry.content textilize(post.body), :type => :html
      entry.updated post.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")
      entry.author do |author|
        author.name post.admin.username
        author.email post.admin.email
      end
    end if post.published
  end
end
