class Preview

  def self.call(env)
    data = Rack::Request.new(env).params["data"]
    [200, {"Content-Type" => "text/plain"}, [textilize(data)]]
  end

end
