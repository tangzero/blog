# Read about factories at http://github.com/thoughtbot/factory_girl

Factory.define :post do |f|
  f.title "Post title"
  f.body "Post body"
  f.published false
end
