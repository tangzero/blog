require 'spec_helper'

describe Post do
  should_belong_to :admin
  should_validate_presence_of :title, :body

  context "Tagged Posts" do
    before :all do
      @post = Factory(:post, :tag_list => ["faketag"])
    end

    it "should return posts by tag" do
      @post.should == Post.tagged("faketag").first
    end

    it "should not return posts when searching for invalid tag" do
      Post.tagged("nothing").should have(0).items
    end
  end

  it "should return posts in order of creation" do
    posts = [Factory(:post), Factory(:post)].reverse
    posts.should == Post.recent
  end

end
