Blog::Application.routes.draw do
  devise_for :admins, :path => "admin", :path_names => { :sign_in => "login", :sign_out => "logout" }
  resources :posts
  post "preview" => Preview
  root :to => "posts#index"
end
